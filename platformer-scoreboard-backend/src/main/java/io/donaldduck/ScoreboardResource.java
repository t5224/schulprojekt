package io.donaldduck;

import java.net.URI;
import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/scores")
public class ScoreboardResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listScores() {
        return Response.ok(Score.returnScores()).build();
    }

    @POST
    @Transactional
    public Response receiveScore(Score score) 
    {
        score.persist();
        
        return Response.created(URI.create("/scores/" + score.id)).build();
    }
}
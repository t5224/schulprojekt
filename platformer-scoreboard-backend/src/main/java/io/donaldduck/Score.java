package io.donaldduck;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Entity;
import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity
public class Score extends PanacheEntity {
    
    public String time; 
    
    public String name; 

    public String date;

    public static List<Score> returnScores() {
        
        List<Score> scores = Score.listAll();
        scores.sort(new Comparator<Score>() {

            @Override
            public int compare(Score arg0, Score arg1) {
                
                String[] arg0Split = arg0.getPlayTime().split(":");
                String[] arg1Split = arg1.getPlayTime().split(":");
                for(int fieldNr = 0; fieldNr < arg0Split.length && fieldNr < arg1Split.length; fieldNr++)
                {
                    int compare = Integer.valueOf(arg0Split[fieldNr]).compareTo( Integer.valueOf(arg1Split[fieldNr] ));
                    if(compare != 0) 
                    {
                        return compare;
                    }
                }
                return 0;
            }
        });
        
        
        return scores;
    }

    private String getPlayTime() 
    {
        return this.time;
    }


}

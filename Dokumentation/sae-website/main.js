window.onload = function() {
  const xhr = new XMLHttpRequest();
  const url ="https://donaldduck.io:8443/scores"; //replace with server path later
  xhr.open('GET', url);
  xhr.onreadystatechange = function () {
    if(xhr.readyState == 4) {
      const scores = JSON.parse(xhr.response);
      let x = 1;
      scores.forEach(function (item) {
        const scoreboardTable = document.getElementById("scoreboard-table");
        var row = scoreboardTable.insertRow(-1);
        var cellPlacement = row.insertCell(0);
        var cellName = row.insertCell(1);
        var cellTime = row.insertCell(2);
        var cellDate = row.insertCell(3);
        cellPlacement.innerHTML = x;
        cellName.innerHTML = item.name;
        cellTime.innerHTML = item.time;
        cellDate.innerHTML = item.date;
        x++;
      });
    }  
  };
  xhr.send(null);
};

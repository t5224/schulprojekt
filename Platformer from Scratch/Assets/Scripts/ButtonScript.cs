using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartGame : MonoBehaviour
{   
    public GameObject hud;
    public string playerName;

    public Text inputText; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnGameStart()
    {
        playerName = inputText.text;
        hud.SetActive(false);

        Timer timer = GameObject.FindGameObjectWithTag("Timer").GetComponent<Timer>();
        timer.StartTimer();
    }

    
}

using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    public float playerPos;
    public int initialcamBP = 5;
    public int camBreakpoint = 5;
    public int oldCamBreakpoint = -5;
    public int currBP = 1;

    private void Awake()
    {
        player = GameObject.Find("Player");
    }

    private void Update()
    {
        playerPos = player.transform.position.y;

        if (playerPos > camBreakpoint) 
        {
            gameObject.transform.position = new Vector3(0, 10 * currBP, -10);
            oldCamBreakpoint = -5 + 10 * currBP;
            camBreakpoint = 5 + 10 * currBP;
            currBP++;
        } else if(playerPos < oldCamBreakpoint)
        {
            gameObject.transform.position = new Vector3(0, 10 * (currBP - 2), -10);
            oldCamBreakpoint -= 10;
            camBreakpoint -= 10;
            currBP--;
        }
    }
}

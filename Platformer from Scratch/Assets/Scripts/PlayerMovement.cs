using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    //SerializeField and public variables can be changed in Unity-Editor
    [SerializeField] private float speed;
    [SerializeField] private float maxJumpHeight;
    [SerializeField] private bool isGrounded;
    [SerializeField] private bool isOnIncline;

    public bool isFrozen = true;

    private bool wasSpacePressed = false;
    private bool wasSpaceReleased = false;
    private float jumpCharge = 0.10f;
    private string lastIncline = "noIncline";

    private float timer;
    private float increments = 0.03f;

    //reference to player object
    private Rigidbody2D body;
    private Animator anim;


    private RaycastHit2D wallCheckHit;

    //reference to other GameObjects
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private LayerMask inclineMask;

    //awake is executed on loading the script
    private void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    //update is executed every frame
    private void Update()
    {   
        if(isFrozen)
        {
            return;
        }

       
       
        //detects if players bottom touches ground
        isOnIncline = Physics2D.OverlapCircle(new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - 0.5f), 0.35f, inclineMask);
        isGrounded = Physics2D.OverlapBox(new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - 0.5f), new Vector2(0.7f, 0.1f), 0f, groundMask);
        
        // RaycastHit2D wallHit = Physics2D.Raycast(new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - 0.5001f), new Vector2(0.7f, 0.1f), 0f, groundMask);
        // if(wallHit.normal.x < -0.5) 
        // {
        //     isGrounded = false;
        // }

        wallCheckHit = Physics2D.Raycast(gameObject.transform.position, gameObject.transform.right, 0.5f, groundMask);
        if(wallCheckHit)
        {
            isGrounded = false; 
        }

        wasSpacePressed = Input.GetKey(KeyCode.Space);
        wasSpaceReleased = Input.GetKeyUp(KeyCode.Space);

        //state-changer
        if(wasSpacePressed)
        {
            chargeJump();
            anim.SetBool("isRunning", false);
            anim.SetBool("isGrounded", isGrounded);
        } else if(wasSpaceReleased)
        { 
            jump();
        } else if(isOnIncline && !isGrounded)
        {
            inclineMovement();
        } else if(!isGrounded)
        {
            inAirMovement();
        } else if(isGrounded)
        {
            groundedMovement();
            anim.SetBool("isRunning", Input.GetAxisRaw("Horizontal") != 0);
        }
        
        //flip character sprite
        if (body.velocity.x > 0f)    //horizontalInput or body.velocity.x
        {
            transform.localScale = Vector2.one;
        }
        else if (body.velocity.x < 0f)
        {
            transform.localScale = new Vector2(-1, 1);
        }


        if(Input.GetKeyDown(KeyCode.R)) 
        {
                         SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

    }

    private void groundedMovement()
    {
        if(!wasSpacePressed)
        {
            if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
            {
                body.velocity = new Vector2(0, 0);
            }
            else
            {
                body.velocity = new Vector2(Input.GetAxisRaw("Horizontal") * speed, body.velocity.y);
            }
        }  
    }

    private void inAirMovement()
    {
        lastIncline = "noIncline";
    }

    private void inclineMovement()
    {
        RaycastHit2D hit = Physics2D.Raycast(new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - 0.5001f), -Vector2.up);
        if(lastIncline == "noIncline")
        {
            if (hit.normal.x > 0)
            {
                lastIncline = "rightIncline";
                body.velocity = new Vector2(speed, -10);
            }
            else if (hit.normal.x < 0)
            {
                lastIncline = "leftIncline";
                body.velocity = new Vector2(-speed, -10);
            }
            else if (hit.normal.x == 0 && !isGrounded)
            {
                body.velocity = new Vector2(body.velocity.x, -10);
            }
        } else if(lastIncline == "rightIncline")
        {
            body.velocity = new Vector2(speed, -10);
        } else if(lastIncline == "leftIncline")
        {
            body.velocity = new Vector2(-speed, -10);
        }
    }

    private void jump()
    {
        if(isGrounded )
        {
            body.velocity = new Vector2(Input.GetAxisRaw("Horizontal") * speed, maxJumpHeight * jumpCharge);
            anim.SetTrigger("jump");
            jumpCharge = 0.10f;
        }
    }

    private void chargeJump()
    {
        if(isGrounded)
        {
            body.velocity = new Vector2(0, 0);
            timer += Time.deltaTime;
            //for every increment of a second add 5% charge
            if(timer >= increments)
            {
                timer -= timer;
                if (jumpCharge < 1f)
                {
                    jumpCharge += 0.05f;
                }
            }
        }
    }
    public float getJumpCharge()
    {
        return jumpCharge;
    }

    public void unfreeze()
    {
        isFrozen = false;
    }

    public void freeze()
    {
        isFrozen = true; 
    }
}


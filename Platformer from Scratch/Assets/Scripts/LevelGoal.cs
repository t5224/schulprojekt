using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelGoal : MonoBehaviour
{

    public bool isLevelCompleted; 
    public Sprite finImg;
    // Start is called before the first frame update
    void Start()
    {
        isLevelCompleted = false;
    }

    // Update is called once per frame
    void Update()
    {
 

    }
 
   void OnTriggerEnter2D(Collider2D other)
   {
      if(other.gameObject.name == "Player")
      {
          Timer timer = GameObject.FindGameObjectWithTag("Timer").GetComponent<Timer>();
          timer.StopTimer();

          ScorePublisher publisher = GameObject.FindGameObjectWithTag("ScorePublisher").GetComponent<ScorePublisher>();
          publisher.PublishScore();

          PlayerMovement player = GameObject.Find("Player").GetComponent<PlayerMovement>();
            Rigidbody2D rigidbody2D = GameObject.Find("Player").GetComponent<Rigidbody2D>();
          player.freeze();
          rigidbody2D.gravityScale = 0.0f; 
          rigidbody2D.velocity = new Vector2(0f,0f);
          
          Image bgImg = GameObject.Find("Background-Image").GetComponent<Image>();
          bgImg.sprite = finImg;

          isLevelCompleted = true;
      }
   }    
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class jumpChargeUi : MonoBehaviour
{
    private GameObject player;
    private PlayerMovement playerMovement;
    private float playerJumpCharge = 0.1f;
    [SerializeField]private Image filling;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        playerMovement = player.GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        playerJumpCharge = playerMovement.getJumpCharge();
        if(playerJumpCharge > 1f)
        {
            filling.fillAmount = 1f;
        } else
        {
            filling.fillAmount = (playerJumpCharge - 0.1f) * 10 / 9;
        }
    }
}

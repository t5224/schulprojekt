using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class ScorePublisher : MonoBehaviour
{
    public void PublishScore()
    {
        string json = BuildJson(); 
        
        StartCoroutine(Post("https://donaldduck.io:8443/scores", json));
        
    }
    private string BuildJson()
    {
        Timer timer = GameObject.FindGameObjectWithTag("Timer").GetComponent<Timer>();
        PlayerName name = GameObject.FindGameObjectWithTag("PlayerName").GetComponent<PlayerName>();
        string json = "{\"time\": \""+ timer.timePlayedString + "\",\"name\": \"" + name.inputText.text + "\", \"date\": \" "+ CurrentDate() +"\"}";
        Debug.Log(json);
        return json;
    }

    private string CurrentDate() 
    {
        return System.DateTime.Now.ToString("MM/dd/yyyy");    
    }


    private IEnumerator Post(string url, string bodyJsonString)
    {
        Debug.Log("Posting");
        
        UnityWebRequest unityWebRequest = new UnityWebRequest(url, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(bodyJsonString);
        unityWebRequest.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        unityWebRequest.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        unityWebRequest.SetRequestHeader("Content-Type", "application/json");
        yield return unityWebRequest.SendWebRequest();
        Debug.Log("Status Code: " + unityWebRequest.responseCode);
    }
}
 
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{

    public Text timerText; 

    public string timePlayedString;
    private bool timerGoing;

    private float elapsedTime;

    // Start is called before the first frame update
    void Start()
    {
        timerText.text = "00:00:00";
        timerGoing = false; 
        elapsedTime = 0f; 
    }

    public void StartTimer()
    {
        timerGoing = true;
    }

    public void StopTimer() 
    {
        timerGoing = false; 
    }
    void Update()
    {
       if(timerGoing) 
       {
            elapsedTime += Time.deltaTime; 
            timePlayedString = TimeSpan.FromSeconds(elapsedTime).ToString("mm':'ss':'ff");
            timerText.text = timePlayedString;
       }
        
    }

}
